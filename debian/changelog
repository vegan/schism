schism (2:20240308-1) unstable; urgency=medium

  * New upstream version 20240308
  * Use the default ci config from salsa

 -- Dennis Braun <snd@debian.org>  Mon, 11 Mar 2024 22:18:45 +0100

schism (2:20230906-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Use secure URI in Homepage field.
  * Update standards version to 4.6.2, no changes needed.

  [ Dennis Braun ]
  * New upstream version 20230906

 -- Dennis Braun <snd@debian.org>  Fri, 15 Sep 2023 21:47:26 +0200

schism (2:20221201-1) unstable; urgency=medium

  * New upstream version 20221201
  * d/control:
    + Apply wrap-and-sort -ast
    + Add myself as uploader
    + Remove obsolete libsdl1.2-dev B-D
    + Mark python3 B-D as native to fix cross-building
  * Add lintian-overrides
  * d/rules:
    + Add hardening
    + Remove obsolete dh flag --with-autoreconf, it's default
  * Add salsa ci config

 -- Dennis Braun <snd@debian.org>  Sun, 04 Dec 2022 11:37:33 +0100

schism (2:20221020-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <tar@debian.org>  Mon, 24 Oct 2022 07:46:50 +0200

schism (2:20220905-1) unstable; urgency=medium

  * New upstream version.
  * d/copyright: bump years.
  * Bump standards version to 4.6.1.

 -- Gürkan Myczko <tar@debian.org>  Mon, 12 Sep 2022 10:41:30 +0200

schism (2:20220807-2) unstable; urgency=medium

  * d/control: Add libsdl2-dev to build-depends.

 -- Gürkan Myczko <tar@debian.org>  Sun, 14 Aug 2022 16:30:07 +0200

schism (2:20220807-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <tar@debian.org>  Sun, 14 Aug 2022 12:33:42 +0200

schism (2:20220506-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <tar@debian.org>  Fri, 13 May 2022 16:26:58 +0200

schism (2:20220125-1) unstable; urgency=medium

  * New upstream version.
  * d/watch: drop template part.

 -- Gürkan Myczko <tar@debian.org>  Tue, 25 Jan 2022 20:35:02 +0100

schism (2:20211116-1) unstable; urgency=medium

  * New upstream version.
  * Bump standards version to 4.6.0.
  * Update maintainer address.

 -- Gürkan Myczko <tar@debian.org>  Wed, 17 Nov 2021 14:17:53 +0100

schism (2:20210525-2) unstable; urgency=medium

  * Upload to unstable.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Sun, 15 Aug 2021 13:02:07 +0200

schism (2:20210525-1) experimental; urgency=medium

  * New upstream version.
  * Bump standards version to 4.5.1.
  * Bump debhelper version to 13, drop d/compat.
  * d/upstream/metadata: added.
  * d/copyright: modernised and updated copyright years.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Wed, 26 May 2021 13:24:41 +0200

schism (2:20200412-1) unstable; urgency=medium

  * New upstream version.
  * Bump standards version to 4.5.0.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Fri, 15 May 2020 07:35:28 +0200

schism (2:20190805-3) unstable; urgency=medium

  * Team upload.
  * Use Python 3 to build (Closes: #938442)
  * Add Rules-Requires-Root: no.

 -- Stuart Prescott <stuart@debian.org>  Tue, 14 Jan 2020 16:52:28 +1100

schism (2:20190805-2) unstable; urgency=medium

  * Bump standards version to 4.4.1.
  * Fix Vcs fields.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Thu, 14 Nov 2019 12:39:43 +0100

schism (2:20190805-1) unstable; urgency=medium

  * New upstream version. (Closes: #933807, #933808, #933809)
    - fmt_mtm_load_song in fmt/mtm.c (CVE-2019-14465)
    - heap-based buffer overflow via a large number of song patterns
      in fmt_mtm_load_song in fmt/mtm.c (CVE-2019-14524)
    - integer underflow via a large plen in fmt_okt_load_song in the
      Amiga Oktalyzer parser in fmt/okt.c (CVE-2019-14523)

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Tue, 06 Aug 2019 11:33:06 +0200

schism (2:20190722-1) unstable; urgency=medium

  * New upstream version.
  * debian/patches: dropped.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Sun, 28 Jul 2019 20:55:17 +0200

schism (2:20190614-2) unstable; urgency=medium

  * Upload to unstable.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Sun, 07 Jul 2019 12:16:23 +0200

schism (2:20190614-1) experimental; urgency=medium

  * New upstream version.
  * debian/control: update Vcs links.
  * Bump standards version to 4.2.1.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Mon, 17 Jun 2019 09:47:51 +0200

schism (2:20181223-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Mon, 24 Dec 2018 21:37:30 +0100

schism (2:20180810-2) unstable; urgency=medium

  * Patch fixing build on GNU/Hurd.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Tue, 11 Dec 2018 09:58:10 +0100

schism (2:20180810-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Tue, 14 Aug 2018 14:39:18 +0200

schism (2:20180513-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Mon, 14 May 2018 12:53:28 +0200

schism (2:20180209-1) unstable; urgency=medium

  * New upstream version.
  * Bump debhelper version to 11.
  * Bump standards version to 4.1.3.
  * debian/watch: updated.
  * debian/menu: dropped.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Mon, 19 Feb 2018 07:02:45 +0100

schism (2:20170910-1) unstable; urgency=medium

  * New upstream version.
  * Bump debhelper version to 10.
  * Bump standards version to 4.1.1.
  * debian/control: update Suggests.
  * debian/install: added to install desktop files.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Tue, 24 Oct 2017 10:25:48 +0200

schism (2:20160521-1) unstable; urgency=low

  * New upstream version. (Closes: #730571)
  * Bump standards version to 3.9.8.
  * Drop debian/patches, not needed anymore.
  * Drop debian/schism.1 and manpages, upstream ships a better one.
  * debian/rules: add debian hardening for schismtracker.
  * Upstream renamed schism binary to schismtracker.
  * Updated debian/menu.
  * Change my name.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Tue, 26 Jul 2016 09:53:37 +0200

schism (2:0+20110101-1) unstable; urgency=low

  * New upstream snapshot.
  * Refresh 01-binutils_gold.patch.
  * Update debian/copyright.

 -- Alessio Treglia <alessio@debian.org>  Fri, 18 Feb 2011 15:20:59 +0100

schism (2:0+20100202-2) unstable; urgency=low

  * Upload to unstable.

 -- Alessio Treglia <alessio@debian.org>  Wed, 09 Feb 2011 21:19:39 +0100

schism (2:0+20100202-1) experimental; urgency=low

  * New upstream release.
  * Build-Depends on Python.
  * debian/rules:
    - Create auto directory before calling dh_auto_build in order to prevent
      FTBFS.
    - Install NEWS file as upstream's changelog.
  * Refresh patches.
  * Update debian/gbp.conf file.

 -- Alessio Treglia <alessio@debian.org>  Sun, 05 Sep 2010 20:29:56 +0200

schism (2:0+20090817-2) unstable; urgency=low

  * Update Maintainer:
    - The Debian Multimedia team is currently merging with the Debian
       Multimedia Maintainers team (http://wiki.debian.org/DebianMultimedia).
    - Add myself as uploader.
  * debian/control:
    - Wrap long lines.
    - Add Vcs-* tags.
    - Update Standards to 3.9.0.
  * Drop automake from Build-Depends field.
  * Drop changes to upstream sources from the packaging.
  * Set format to 3.0 (quilt).
  * Switch to debhelper 7 + autoreconf add-on.
  * Add watch file.
  * Add debian/gbp.conf file.
  * Add .gitignore file.
  * debian/copyright: Fix copyright-refers-to-symlink-license warning.
  * Add patch to fix FTBFS with binutils-gold (Closes: #556337).

 -- Alessio Treglia <alessio@debian.org>  Sat, 10 Jul 2010 12:33:40 +0200

schism (2:0+20090817-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Drop build-dependency on libxxf86misc-dev (closes: #559692)

 -- Julien Cristau <jcristau@debian.org>  Wed, 27 Jan 2010 14:44:42 +0100

schism (2:0+20090817-1) unstable; urgency=low

  * New upstream version.
  * Bump standards version.
  * Drop dh_desktop call.
  * Updated homepage.

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Mon, 17 Aug 2009 21:42:51 +0200

schism (2:0+20090126-1) unstable; urgency=low

  * New upstream version.
  * Bump debhelper version.
  * Update debian/copyright.
  * Removed build dependency libxkbsel-dev.
  * Changed recommends and updated it to suggests. (Closes: #476898)

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Thu, 29 Jan 2009 10:59:59 +0100

schism (2:0+20080403-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix bashism in debian/rules (Closes: #478625)
  * Bump Standards-Version to 3.8.0.

 -- Chris Lamb <chris@chris-lamb.co.uk>  Sat, 07 Jun 2008 22:08:45 +0100

schism (2:0+20080403-1) unstable; urgency=low

  * New upstream version.
  * Remove Simon from Uploaders.
  * Updated my email address.
  * Recommend xmms2-plugin-modplug instead of xmms-modplug, which is no longer
    available. (Closes: #473835)

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Sun, 06 Apr 2008 10:21:33 +0200

schism (2:0+20080222-1) unstable; urgency=low

  * New upstream.
  * Added Debian Multimedia to Uploaders.
  * debian/copyright: Updated years.
  * debian/control: Extended Recommends.
  * debian/rules: Call dh_desktop.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Fri, 22 Feb 2008 12:19:06 +0100

schism (2:0+20071208-1) unstable; urgency=low

  * New upstream version, fixes GNU/kFreeBSD build problems.
  * debian/control:
    + Updated build-depends. (Closes: #453235)
    + Updated standards version.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Sat, 08 Dec 2007 20:45:28 +0100

schism (2:0+20071117-1) unstable; urgency=low

  * New upstream version.
  * debian/copyright: Reformatted.
  * debian/rules: Install desktop and icon files.
  * debian/menu: Added font editor entry.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Sat, 17 Nov 2007 14:22:42 +0100

schism (2:0+20071026-1) unstable; urgency=low

  * New upstream version.
  * debian/menu: Update for the new policy.
  * debian/compat: Bumped, no changes needed.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Fri, 26 Oct 2007 21:30:25 +0200

schism (1:0.5rc1-1) unstable; urgency=low

  * New upstream version.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Tue, 16 Jan 2007 15:41:29 +0100

schism (2+20061221-1) unstable; urgency=low

  * New upstream tarball.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Fri, 22 Dec 2006 10:52:59 +0100

schism (2+20061016-1) unstable; urgency=low

  * New upstream version. (Closes: #376836)
  * New upstream author, updated homepage.
  * New maintainer, with Simons' approval.
  * Bump debian standards version.
  * Added to menu.
  * Updated FSF address in debian/copyright.
  * Created a manual page.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Thu, 26 Oct 2006 12:51:21 +0200

schism (0.2a-3) unstable; urgency=low

  * Build with gcc 4.0+ fixed (Closes: #314816)

 -- Simon Richter <sjr@debian.org>  Thu, 19 Jan 2006 01:25:37 +0100

schism (0.2a-2) unstable; urgency=low

  * Using fixed SysV types instead of platform dependent ones

 -- Simon Richter <sjr@debian.org>  Sun, 22 May 2005 20:09:28 +0200

schism (0.2a-1) unstable; urgency=low

  * Initial release.

 -- Simon Richter <sjr@debian.org>  Sun, 22 May 2005 12:44:50 +0200
